<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/list_users', function () {
    $results = app('db')->select("SELECT * FROM users");
    return response()->json($results);
});

$router->post('/add_users', function(Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $password = app('hash')->make($request->input("password"));
    $fullname = $request->input("fullname");
    $phone = $request->input("phone");
    $account = $request->input("account");

    $query = app('db')->insert('INSERT into users
					(Username, Password, fullname, phone, account)
					VALUE (?, ?, ?, ?, ?)',
					[ $username,
					  $password,
					  $fullname,
					  $phone,
					  $account] );
	return "Ok";
});
$router->post('/add_items', function(Illuminate\Http\Request $request) {
    $item_name = $request->input("itemname");
    $item_price = $request->input("itemprice");
    $UserID = $request->input("UserID");
    $item_id = $request->input("itemid");

    $query = app('db')->insert('INSERT into accounts
					(UserID, Item_name, item_price, item_id)
					VALUE (?, ?, ?, ?)',
					[ $UserID,
					  $item_name,
					  $item_price,
					  $item_id ] );
	return "Ok";
});
$router->put('/update_users', function(Illuminate\Http\Request $request) {
    $item_name = $request->input("itemname");
    $item_price = $request->input("itemprice");
    $UserID = $request->input("UserID");
    $item_id = $request->input("item_id");

    $query = app('db')->update('UPDATE accounts
								SET item_name=?,
                                    item_price=?,
                                    item_id=?
								WHERE
									UserID=?',
								[$item_name,
                                 $item_price,
                                 $item_id,
                                 $UserID ] );
	return "ok";
});
$router->delete('/delete',function(Illuminate\Http\Request $request){
	$UserID = $request->input("UserID");
	
	$query = app('db')->delete('DELETE from accounts
								WHERE
									UserID=?',
									[$UserID]);
	return ("ok");
	
});
//